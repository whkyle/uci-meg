#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "Riostream.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
using namespace std;
//gSystem->SetBatch(true);
double zmin = 10000;
double zmax = -10000;
double phimin = 10000;
double phimax = -10000;
// these define zmin and zmax originally to be very large, therefore immediately being replaced by the first entry

vector<int> validPM_v;                                                 // valid PM number
unordered_map<int,int> ScalerCh_map;
vector<double> PMZ_v;                                                  // valid PM Z position 
vector<double> PMPhi_v;// valid PM phi position
vector<double>TRGGain_v;
vector<double> PMcharge_v;
vector<double> PMchargeraw_v;                                             // valid PM charge
std::unordered_map<Int_t, Int_t> ChTRGGainmap;
vector<int> irradPM_v;
int Nvalidpm=0;                                                        // number of valid PMs
int Nenabled=0;// number of PMs being analyzed
int Nnegative= 0;
int Nmiddle=0;
int Nside=0;
int Nevents=0;
const int Nmppc = 4092;                                                // number of total MPPCs set in GetPMdata
int badEventCounter=0;                                                 // counts number of instances bad xray source data required skipping event
double leftch=0;
double rightch=0;
double skew=0;
int coherentnoise=0;
int allbigcount=0;
int wrongplacecount=0;
double centerscancoord=0;
int sqpos;
int supergausfitpower = 10;


TString  RecWFFilePath(int run){return (string)getenv("MEG2SYS")+Form("/analyzer/recfiles/rec%06d.root",run);} 
TString  RecTRFilePath(int run){return (string)getenv("MEG2SYS")+Form("/analyzer/recfiles/rec%06d.root",run);}

void   xray_ana(int run, int scan,bool WFAnalysis, bool TRAnalysis);    // main routine from Terence, original 
void   getPMdata(int run,bool WFAnalysis, bool TRAnalysis);             // gets the number of valid MPPCs, fills some arrays
double XYZ2Phi(double x, double y, double /* z */);                     // convert xyz to phi
bool   IsXrayEvent(double Nsum2);                                       // logical defining a true x-ray event
double TwoGaus(double *x,double *par);
Double_t WeightedMean(TGraphErrors* gr);
Double_t SuperGaus(Double_t *x,Double_t *par);

// two gaussian fit to MPPC profile (unused)




//-----------main rountine, input run number and whether z scan (0) or phi scan (1)--------
void xWill(int runnum, int scan) {
  gStyle->SetFuncColor(kRed);
  bool WFAnalysis=true;
  bool TRAnalysis=false;
  getPMdata(runnum, WFAnalysis, TRAnalysis);            // finds trigger MPPCs
  xray_ana(runnum, scan, WFAnalysis, TRAnalysis);
  cout << " number of events in middle " << Nmiddle << " number of events on side " << Nside << " number of events " << Nevents << endl;
// does the analysis
  return;
}  //----------------end main routine------------------------------------------------------




//----------actual analysis routine: run number, z or phi scan, waveform or trigger ana----
void xray_ana(int run, int scan, bool WFAnalysis, bool TRAnalysis) {

  char name[200];
  char fitres[100];

  //--------------------------------Initialization------------------------------
  gStyle->SetTitleW(2);
  TClonesArray* cagrCharge      = new TClonesArray("TGraphErrors",Nvalidpm);
  TClonesArray* cahsignal       = new TClonesArray("TH1D"        ,Nvalidpm); // * indicates that lhs (cahsignal) is a pointer to an array; new indicates this is a pointer
  TClonesArray* cahsignald       = new TClonesArray("TH1D"        ,Nvalidpm);
  // TClonesArray cahsignal       = new TClonesArray("TH1D"        ,Nvalidpm); //  is wrong because lhs is not a pointer, rhs is a pointer
// TClonesArray cahsignal       = TClonesArray("TH1D"        ,Nvalidpm);     //  is wrong because lhs is a pointer, rhs is a pointer   (can try this)
  TClonesArray* detectedcharge  = new TClonesArray("TH1D"        ,Nvalidpm);
  TClonesArray* cagrTrigger     = new TClonesArray("TGraphErrors",Nvalidpm);
  TClonesArray* caftg           = new TClonesArray("TF1"         ,Nvalidpm);
  TGraph*       grBeamPosition  = new TGraph();
  TClonesArray* cagrXray        = new TClonesArray("TGraphErrors",Nvalidpm);
  

  //------Define a bunch of histogram arrays indexed by valid MPPC--------------
  for (vector<int>::iterator it=validPM_v.begin();it!=validPM_v.end();it++) {
    int dis=distance(validPM_v.begin(),it);     // essentially the index in the stack

    new((*cagrCharge    )[dis]) TGraphErrors();
    if (scan ==1)
      new((*cahsignal     )[dis]) TH1D(Form("mppc%d",*it),Form("mppc%d",*it),zmax - zmin + 30,zmin -10,zmax + 20);  // z or phi of x-ray when MPPC has max charge
    else
      new((*cahsignal     )[dis]) TH1D(Form("mppc%d",*it),Form("mppc%d",*it),(phimax - phimin + 4)/0.05,phimin -2,phimax + 2);  // z or phi of x-ray when MPPC has max charge

    if (scan ==1)
      new((*cahsignald     )[dis]) TH1D(Form("fitdiff_mppc%d",*it),Form("mppc%d",*it),zmax - zmin + 30,zmin -10,zmax + 20);  // z or phi of x-ray when MPPC has max charge
    else
      new((*cahsignald     )[dis]) TH1D(Form("fitdiff_mppc%d",*it),Form("mppc%d",*it),(phimax - phimin + 4)/0.05,phimin -2,phimax + 2);  // z or phi of x-ray when MPPC has max charge

    // 20 is added on both sides, chosen to have some wiggle room on the edges of the triggered mppcs
    // * indicates that we are getting the object rather than the array of pointers
    if (scan ==1 )
      new((*detectedcharge)[dis]) TH1D(Form("charge%d",*it),Form("charge%d",*it),zmax - zmin + 40, 0.,2.);    // charge when MPPC has max charge
    else
      new((*detectedcharge)[dis]) TH1D(Form("charge%d",*it),Form("charge%d",*it), 100, 0.,10.);    // charge when MPPC has max charge
    int ich= distance(validPM_v.begin(),it);//scaler ch
    
    TF1 *myfit = new TF1("myfit", "[0]*TMath::Exp(-( TMath::Power( (x - [1])/[2] , 10)))", zmin -10 , zmax+ 20);
    myfit->SetParName(0,"A");
    myfit->SetParName(1,"x0");
    myfit->SetParName(2,"denom");
    
    myfit->SetParameter(0, 50);
    myfit->SetParameter(1, -99);
    myfit->SetParameter(2, 10);
    
    
    new((*cagrTrigger   )[dis]) TGraphErrors();
    new((*caftg         )[dis]) TF1(Form("ftg%d",*it),TwoGaus,-400,400,5);
    new((*cagrXray      )[dis]) TGraphErrors();
  }  //-----------------end loop on valid MPPCs---------------------------------
  
  sprintf(name,"xray_r%d.root",run);         // set up output file name
  TFile *fout =new TFile(name,"RECREATE");   // set up output file
  TTree* tout = new TTree("tree","tree");    // set up an output tree
  TNtuple *fittuple = new TNtuple("ntuple","FitRes","zlisted:philisted:idnum:runnum:fitcenter:fitNDF:fitChiSquared:fitProb");    // set up an output ntuple for fitting results
  vector<int>    brchannel;     // 
  vector<double> brcharge;
  vector<double> brheight;

  Float_t zlisted,philisted,idnum,runnum,fitcenter,fitNDF,fitChiSquared,fitProb;
  runnum = (Float_t) run;
  
  tout->Branch("channel",&brchannel);        // branch with MPPC channel
  tout->Branch("charge" ,&brcharge);         // branch with integrated charge
  tout->Branch("height" ,&brheight);         // branch with max amplitude in waveform 
  
  int nEvent=0;
  
  TBranch* bxraydata    = 0;
  TBranch* btrgscaler   = 0;
  TBranch* bxecwfcl     = 0;
  TBranch* bxecfastrec  = 0;
  TBranch* beventheader = 0;
  TClonesArray*   XECFastRecResult          = new TClonesArray("MEGXECFastRecResult");
  TClonesArray*   XECWaveformAnalysisResult = new TClonesArray("MEGXECWaveformAnalysisResult");
  TClonesArray*   recTRGScaler              = new TClonesArray("MEGTRGScaler");
  MEGXRAYData*    recXRAYData               = new MEGXRAYData();
  MEGEventHeader* recEventHeader            = new MEGEventHeader();
  
  TFile* fWFrec;
  TFile *fTRrec;
  TTree* WFrec;
  TTree *TRrec;

  //------------Define rec file and tree to be read, set addresses of branches---------------
  fWFrec = new TFile(RecWFFilePath(run).Data(),"READ");     // Rec waveform tree
  WFrec = (TTree*)fWFrec->Get("rec");
  
  nEvent = WFrec->GetEntries();                             // number of waveform events
  cout<<"Read rec "<<run<<". "<<nEvent<<" events."<<endl;
  
  bxraydata = WFrec->GetBranch("xraydata.");                // x-ray source data (e.g. x, phi, qpd, etc)
  bxraydata->SetAddress(&recXRAYData);
  
  bxecwfcl = WFrec->GetBranch("xecwfcl");                   // waveforms
  bxecwfcl->SetAddress(&XECWaveformAnalysisResult);
  
  bxecfastrec = WFrec->GetBranch("xecfastrec");             // fastrec analysis total number of pe (unused)
  bxecfastrec->SetAddress(&XECFastRecResult);

  beventheader = WFrec->GetBranch("eventheader.");          // event header
  beventheader->SetAddress(&recEventHeader);
  
  fTRrec = new TFile(RecTRFilePath(run).Data(),"READ");     // Rec trigger tree
  TRrec = (TTree*)fTRrec->Get("rec");
  
  nEvent = TRrec->GetEntries();                             // number of trigger events
  cout<<"Read rec "<<run<<". "<<nEvent<<" events."<<endl;

  //  btgrscaler = TRrec->GetBranch("trgscaler.");              // trigger scalers
  //  btrgscaler->SetAddress(&recTRGScaler);
  
  //  bxraydata = TRrec->GetBranch("xraydata.");                // x-ray information
  //  bxraydata->SetAddress(&recXRAYData);
  
  
  //-------------------------------------- Begin loop on events ---------------------------------------
  double beamZ, beamPhi;
  double beamZ_current= 0.;
  double beamZ_previous= 0.;
  double beamZ_first;
  double beamPhi_current= 0.;
  double beamPhi_previous= 0.;
  double beamPhi_first;
  vector<double>total_time_first;
  vector<double> total_time_last;
  vector<double>live_time_first;
  vector<double> live_time_last;
  vector<double>live_time;
  vector<double> total_time;
  vector<double> weights;
  double live_time_previous = 0;
  double total_time_previous = 0;
  double current_weight=0;
  double bin_number= 0;
  double bin_size;
  if (scan ==1)
    bin_size = 1;
  else
    bin_size = 0.05;
  //need to find this out !!!
  int Nbins=0;
  

  for (int iEvent = 0; iEvent < nEvent; iEvent++) 
    {
      bxraydata    ->GetEntry(iEvent);
      bxecwfcl     ->GetEntry(iEvent);
      bxecfastrec  ->GetEntry(iEvent);
    beventheader ->GetEntry(iEvent);
    //   btrgscaler   ->GetEntry(iEvent);
    
    //--diagnostic print of livetime and total time
    // cout<<"live  time: "<<recEventHeader->Getlivetime()<<endl;
    // cout<<"total time: "<<recEventHeader->Gettotaltime()<<endl;
    
    if (recXRAYData->GetIsGood())
      {
        badEventCounter++;
      }
    
    //--skip event if GetIsGood is true -- this refers to data from the x-ray source, backwards see below
    if (recXRAYData->GetIsGood()) continue;        // this appears to be backwards logic (bad->GetIsTrue() => true)
    
    beamZ   = recXRAYData->GetBeamZ();             // x-ray z position
    beamPhi = recXRAYData->GetBeamPhi();           // x-ray phi position

    //--fill the t-graph with the beam z and beam phi 
    grBeamPosition->SetPoint(grBeamPosition->GetN(), beamZ, beamPhi);    // beamZ is abcissa, beamPHi is the ordinate, GetN() finds and increments the number of points

    //--define Xaxis as either beamPhi or beamZ depending on dir of scan
    double Xaxis(0.);    // define and initialize to 0.
    //double Xaxis[10]={1.,2.,3., ...   10.}  works for arrays
    if (scan==0)    Xaxis = beamPhi;
    else            Xaxis = beamZ;
    
    
    //--Set up some stuff for analysis
    bool isxray    = true;
    double Nsum2   = ((MEGXECFastRecResult*)(XECFastRecResult->At(0)))->Getnsum2();
    isxray         =  IsXrayEvent(Nsum2);
    

    
    // below i seperate into z and phi scans
    // I calculate the total and live time at a given position,
    //then subtract the first and final total and live time to get the ime in each bin
    // then I divide the total time by the live time to get the weight for each bin
    // later the bin number is determined for each event, thus assigning it a weight
    if (scan ==1)
      {
        if (beamZ_current ==0)
          {  beamZ_current = beamZ;
            total_time_first.push_back(recEventHeader->Getlivetime());
            live_time_first.push_back(recEventHeader->Gettotaltime());
            beamZ_first = beamZ;
            cout << " beamZ current started " << endl;
          }
        if (beamZ_current < beamZ && beamZ_current != 0)
          {beamZ_current = beamZ;
            total_time_first.push_back( recEventHeader->Gettotaltime()); //current ;
            live_time_first.push_back( recEventHeader->Getlivetime()); //current;
            total_time_last.push_back(total_time_previous); //previous
            live_time_last.push_back(live_time_previous); //previous
            Nbins++;
            //cout << recEventHeader->Gettotaltime() << " total live time not in stack " << endl;
            
            
          }
        total_time_previous = recEventHeader->Gettotaltime(); //current;
        live_time_previous = recEventHeader->Getlivetime(); //current;
      }
    else
      {
        if (beamPhi_current ==0)
          {  beamPhi_current = beamPhi;
            total_time_first.push_back(recEventHeader->Getlivetime());
            live_time_first.push_back(recEventHeader->Gettotaltime());
            beamPhi_first = beamPhi;
            //cout << " beamZ current started " << endl;
          }
        if (beamPhi_current < beamPhi && beamPhi_current != 0)
          { cout << " beamPhi " << beamPhi << " previous beamPhi " << beamPhi_current << " difference in phi " << beamPhi - beamPhi_current <<  endl;
            beamPhi_current = beamPhi;
            total_time_first.push_back( recEventHeader->Gettotaltime()); //current ;
            live_time_first.push_back( recEventHeader->Getlivetime()); //current;
            total_time_last.push_back(total_time_previous); //previous
            live_time_last.push_back(live_time_previous); //previous
            Nbins++;
            //cout << recEventHeader->Gettotaltime() << " total live time not in stack " << endl;
            
            
          }
        total_time_previous = recEventHeader->Gettotaltime(); //current;
        live_time_previous = recEventHeader->Getlivetime();
        //current;
      }
    }
 unsigned int i = 0;
 for ( i = 0; i < Nbins; i++)
   {
     total_time.push_back(total_time_last[i] - total_time_first[i]);
     live_time.push_back(live_time_last[i] - live_time_first[i]);
     // cout << total_time_first[i] << " total live time in stack (i ) " << endl;
     
   }
 for ( i = 0; i < Nbins; i++)
   weights.push_back(total_time[i]/live_time[i]);
 
 for ( i = 0; i < Nbins; i++)
   cout <<weights[i] << endl;      
 //-----------------------------------------------------------attempt to calculate live time ends--------------------------------
 for (int iEvent = 0; iEvent < nEvent; iEvent++) 
   {
    bxraydata    ->GetEntry(iEvent);
    bxecwfcl     ->GetEntry(iEvent);
    bxecfastrec  ->GetEntry(iEvent);
    beventheader ->GetEntry(iEvent);
    //   btrgscaler   ->GetEntry(iEvent);
    
    //--diagnostic print of livetime and total time
    // cout<<"live  time: "<<recEventHeader->Getlivetime()<<endl;
    // cout<<"total time: "<<recEventHeader->Gettotaltime()<<endl;

    if (recXRAYData->GetIsGood())
      {
        badEventCounter++;
      }
    
    //--skip event if GetIsGood is true -- this refers to data from the x-ray source, backwards see below
    if (recXRAYData->GetIsGood()) continue;        // this appears to be backwards logic (bad->GetIsTrue() => true)

    beamZ   = recXRAYData->GetBeamZ();             // x-ray z position
    beamPhi = recXRAYData->GetBeamPhi();           // x-ray phi position

    //--fill the t-graph with the beam z and beam phi 
    grBeamPosition->SetPoint(grBeamPosition->GetN(), beamZ, beamPhi);    // beamZ is abcissa, beamPHi is the ordinate, GetN() finds and increments the number of points
    
    //--define Xaxis as either beamPhi or beamZ depending on dir of scan
    double Xaxis(0.);    // define and initialize to 0.
    //double Xaxis[10]={1.,2.,3., ...   10.}  works for arrays
    if (scan==0)    Xaxis = beamPhi;
    else            Xaxis = beamZ;
    
    
    //--Set up some stuff for analysis
    bool isxray    = true;
    double Nsum2   = ((MEGXECFastRecResult*)(XECFastRecResult->At(0)))->Getnsum2();
    isxray         =  IsXrayEvent(Nsum2);

    int    maxPMnumber = -1;
    int    maxiCh      = -1;
    double maxcharge   = 0;
    double negativesumcharge = 0;
    double sumcharge   = 0;
    double second_largest_charge = 0;
    double third_largest_charge = 0;
    double negativeavgcharge = 0;
    

    PMcharge_v.clear();
    PMcharge_v.empty();
    PMchargeraw_v.clear();
    PMchargeraw_v.empty();
    
    //---------------loop over the valid MPPCs-------------------------------
    for (vector<int>::iterator it=validPM_v.begin();it!=validPM_v.end();it++) 
      {
        double charge = TMath::Abs(((MEGXECWaveformAnalysisResult*)(XECWaveformAnalysisResult->At(*it)))->GetchargeAt(0));
        int ich= distance(validPM_v.begin(),it);//scaler ch
	if (TRGGain_v[ich]!=0)
	  {
	    PMchargeraw_v.push_back(charge);
	    if (TRGGain_v[ich] == 1) {
	      negativesumcharge += charge;
	      Nnegative++;
	    }
	  }
        
      }

    negativeavgcharge = negativesumcharge/Nnegative;
    for (vector<int>::iterator it=validPM_v.begin();it!=validPM_v.end();it++) 
      {
        int ich= distance(validPM_v.begin(),it);//scaler ch
        double charge = TMath::Abs(((MEGXECWaveformAnalysisResult*)(XECWaveformAnalysisResult->At(*it)))->GetchargeAt(0));
	charge -= (negativeavgcharge);

	if (TRGGain_v[ich]!=0)
	  {
	    PMcharge_v.push_back(charge);
	  }
     
        if (charge>maxcharge) 
          {
        maxPMnumber = *it;
        maxiCh      = distance(validPM_v.begin(),it);
        maxcharge   = charge;
        sumcharge   +=charge;
      }
        if (charge < maxcharge)
          {
          if (charge> second_largest_charge)
            second_largest_charge = charge;
          }
        if (charge < second_largest_charge)
        { if (charge > third_largest_charge)
            third_largest_charge = charge;
        }
      } //----------------end loop on valid MPPCs------------------------------
    if (second_largest_charge/maxcharge < 0.3)
      Nmiddle++;
    if ( (second_largest_charge/maxcharge) < 1 && (second_largest_charge/maxcharge) > 0.8 && (third_largest_charge/maxcharge) < 0.2)
        Nside++;
    Nevents++;
    // cout << " this is beam Z " << beamZ <<" this is first beamZ " << beamZ_first << endl;


    // below I use the weight vector to assign a weight to a given event based on the bin number, which I calculate here 
    if (scan ==1)
      bin_number = round((beamZ - beamZ_first)/bin_size);
    else
      bin_number = round((beamPhi - beamPhi_first)/bin_size);
    unsigned int i = 0;
    //  if( iEvent < 1000 )
    // cout << bin_number <<  " this is bin number " << endl;
    for (i = 0; i< Nbins; i++)
      {
        if (bin_number >( i - 0.1) && bin_number <( i + 0.1) )
          current_weight = weights[i];
      }

    double mppcSepZ = 15.0975;
    double mppcSepPhi = 1.35;
    double zsepinspaces = 8/mppcSepZ;
    double beamPosZspace = 0;
    double phisepinspaces = 8/mppcSepPhi;
    double beamPosPhispace = 0;
    double beampos=0;
    for (int i=0;TRGGain_v[i]!=255;i++){
      beamPosZspace=(beamZ-PMZ_v[i])*zsepinspaces;
      beamPosPhispace=(beamPhi-PMPhi_v[i])*phisepinspaces;
    }
    beamPosZspace+=2;
    beamPosPhispace+=2;
    
    if (scan == 1) {
      beampos = beamPosZspace;
    }
    else {
      beampos = beamPosPhispace;
    }

    
    double maxzbeamdist=mppcSepZ;
    double maxphibeamdist=mppcSepPhi;
    
    //-----------select good events and fill histograms-------------------
    //here is the only cut
    bool sel, sel1, sel2, sel2a, sel2b, sel1a,selbeamZ,selbeamPhi,selwobeam;
    sel1 = (second_largest_charge/maxcharge < 0.15);
    sel1a = (maxcharge > 0.2);
    sel2 = (second_largest_charge/maxcharge > 0.8);
    sel2a = ( third_largest_charge/(maxcharge+second_largest_charge) < 0.15);
    sel2b = ((maxcharge+second_largest_charge) > 0.2);
    selbeamZ= (TMath::Abs(beamZ-PMZ_v[maxiCh])-maxzbeamdist<=0);
    selbeamPhi= (TMath::Abs(beamPhi-PMPhi_v[maxiCh])-maxphibeamdist<=0);
    selwobeam  = (((sel1)&&(sel1a)) || ((sel2)&&(sel2a)&&(sel2b)));
    if (scan==1){
      sel = ((selwobeam)&&(selbeamZ));
      if ((selwobeam)&&!(selbeamZ)){
	wrongplacecount++;
      }
    }
    else{
      sel = ((selwobeam)&&(selbeamPhi));
      if ((selwobeam)&&!(selbeamPhi)){
	wrongplacecount++;
      }
    }

    
    
    
    
    //    if (((second_largest_charge/maxcharge < 0.3)&&(maxcharge>.5)) || ( ( second_largest_charge/maxcharge < 1.) && (second_largest_charge/maxcharge > 0.8) && ( third_largest_charge/maxcharge < 0.2) ) )
    if (selwobeam)
      {   
        ((TGraphErrors*)(*cagrCharge)[maxiCh])->SetPoint(((TGraphErrors*)(*cagrCharge)[maxiCh])->GetN(),Xaxis,maxcharge);
        //((TH1D*)           (*cahsignal)[maxiCh])->Fill(Xaxis);
        ((TH1D*)           (*cahsignal)[maxiCh])->Fill(Xaxis, current_weight); 
        ((TH1D*)           (*cahsignald)[maxiCh])->Fill(Xaxis, current_weight); 

        ((TH1D*)      (*detectedcharge)[maxiCh])->Fill(sumcharge/maxcharge);
      }

    int spacecount =0;
    int tabcount=0;

    if ((iEvent<5000)&&(sel))
      {
	cout <<"Event "<< iEvent << " Charge Array:"<<endl;
	printf("Negative Trigger Avg: %4.3f\n",negativeavgcharge);
	if (scan==1){
	  printf("Beam Z is %4.3f \n", beamZ);
	}
	else {
	  printf("Beam Phi is %4.3f \n", beamPhi);
	}
	for (int it=0;it<=beampos;it++)
	  {
	    printf(" ");
	    spacecount++;
	    if ((spacecount+1)%8<1){
	      tabcount++;
	    }
	  }
	printf("|\t\t\t");
	for (int it=0;it<(3-tabcount);it++){
	  printf("\t");
	}
	for (int it=0;it<=beampos;it++)
	  {
	    printf(" ");
	  }
	printf("|\n");

	printf("%5.2f \t",    PMchargeraw_v[0]);
	printf("%5.2f \t",    PMchargeraw_v[1]);
	printf("%5.2f \t",    PMchargeraw_v[2]);
	printf("%5.2f \t\t\t",PMchargeraw_v[3]);
	
	printf("%5.2f \t",PMcharge_v[0]);
	printf("%5.2f \t",PMcharge_v[1]);
	printf("%5.2f \t",PMcharge_v[2]);
	printf("%5.2f \n",PMcharge_v[3]);

	printf("%5.2f \t",    PMchargeraw_v[4]);
	printf("%5.2f \t",    PMchargeraw_v[5]);
	printf("%5.2f \t",    PMchargeraw_v[6]);
	printf("%5.2f \t\t\t",PMchargeraw_v[7]);
	
	printf("%5.2f \t",PMcharge_v[4]);
	printf("%5.2f \t",PMcharge_v[5]);
	printf("%5.2f \t",PMcharge_v[6]);
	printf("%5.2f \n",PMcharge_v[7]);

	printf("%5.2f \t",    PMchargeraw_v[8]);
	printf("%5.2f \t",    PMchargeraw_v[9]);
	printf("%5.2f \t",    PMchargeraw_v[10]);
	printf("%5.2f \t\t\t",PMchargeraw_v[11]);
	
	printf("%5.2f \t",PMcharge_v[8]);
	printf("%5.2f \t",PMcharge_v[9]);
	printf("%5.2f \t",PMcharge_v[10]);
	printf("%5.2f \n",PMcharge_v[11]);

	printf("%5.2f \t",    PMchargeraw_v[12]);
	printf("%5.2f \t",    PMchargeraw_v[13]);
	printf("%5.2f \t",    PMchargeraw_v[14]);
	printf("%5.2f \t\t\t",PMchargeraw_v[15]);
	
	printf("%5.2f \t",PMcharge_v[12]);
	printf("%5.2f \t",PMcharge_v[13]);
	printf("%5.2f \t",PMcharge_v[14]);
	printf("%5.2f \n\n",PMcharge_v[15]);
      }
    
   } //---------------------------------------------loop over events---------------------------------------
 
 cout << ">>>>>>>>>>>>>>>>>>>>>>>> Number of Events with Bad Xray Source Data = " << badEventCounter << endl;
  
 
 
 //----------------------------section to produce histograms-----------------------------------------------
  TCanvas* cWF= new TCanvas("cWF","cWF",1200,1200);     // canvas for z of MPPC with max charge
  if (Nenabled >=  30) // prints properly dependent on the board size
    cWF->Divide(6,6);
  else
    cWF->Divide(4, 6);
  gStyle->SetTitleSize(0.06,"t");
  int WFvalidch=0;
  int maxbeamlocnum,xmin,xmax;
  if (scan==1){
    maxbeamlocnum=PMZ_v.size()-1;
    xmax=PMZ_v[maxbeamlocnum];
    xmin=PMZ_v[0];
  }
  else{
    maxbeamlocnum=PMPhi_v.size()-1;
    xmax=PMPhi_v[maxbeamlocnum];
    xmin=PMPhi_v[0];
  }
  TF1 *fit = new TF1("fit",SuperGaus,xmin- 20,xmax + 20,4); //define fit func
  double amp,xcenter,w,offset;
  TText *fitlabel = new TText();
  TPaveText *fitout = new TPaveText(.05,.1,.95,.8);
  
  TCanvas* cFitdiffs= new TCanvas("cFitdiff","cFitdiff",1200,1200);     // canvas for z of MPPC with max charge
  cFitdiffs->Divide(4,2);
  
    
  
  //---------------loop on valid MPPCs----------------------------------------
  for (vector<int>::iterator it=validPM_v.begin();it!=validPM_v.end();it++) 
    {
      int dis=distance(validPM_v.begin(),it);
      const int ich = distance(validPM_v.begin(),it);

      sprintf(name,"MPPC %d Z %.1f mm Phi %.2f deg Row %.0f Column %.0f Distance %d",*it,PMZ_v[ich],PMPhi_v[ich],round((PMPhi_v[ich] - phimin)/1.35),round((PMZ_v[ich]- zmin)/15.1), ich );    ((TH1D*)(*cahsignal)[dis])->SetTitle(name);   
      ((TH1D*)(*cahsignal)[dis])->SetTitleSize(0.08);
      ((TH1D*)(*cahsignal)[dis])->GetXaxis()->SetLabelSize(0.08);
      ((TH1D*)(*cahsignal)[dis])->GetYaxis()->SetLabelSize(0.08);
    if (scan ==1)
      ((TH1D*)(*cahsignal)[dis])->SetMaximum(1000);
    else
      ((TH1D*)(*cahsignal)[dis])->SetMaximum(1000);

    ((TH1D*)(*cahsignal)[dis])->SetStats(false);

    fitlabel -> SetTextFont(1);
    fitlabel -> SetTextColor(1);
    fitlabel -> SetTextSize(0.03);
    fitlabel -> SetTextAlign(12);
    fitlabel -> SetTextAngle(0);

    double histmax= ((TH1D*)(*cahsignal)[dis])->TH1::GetBinContent(((TH1D*)(*cahsignal)[dis])->TH1::GetMaximumBin());
    Double_t integral =((TH1D*)(*cahsignal)[dis])->Integral();
    Double_t disinvalid = std::distance(validPM_v.begin(),std::find(validPM_v.begin(),validPM_v.end(),*it));
    
    if (scan==0) {
      ((TF1*)(*caftg)[dis])->SetParameter(0,PMPhi_v[disinvalid]);
      ((TF1*)(*caftg)[dis])->SetParLimits(0,PMPhi_v[disinvalid]-0.5,PMPhi_v[disinvalid]+0.5);
      ((TF1*)(*caftg)[dis])->SetParameter(1,1.2);
      ((TF1*)(*caftg)[dis])->SetParLimits(1,1.0,1.4);
         ((TF1*)(*caftg)[dis])->SetParameter(2,0.1);
         ((TF1*)(*caftg)[dis])->SetParLimits(2,0,0.3);
         
         
    }  
    else{
         ((TF1*)(*caftg)[dis])->SetParameter(0,PMZ_v[disinvalid]);
         ((TF1*)(*caftg)[dis])->SetParLimits(0,PMZ_v[disinvalid]-3,PMZ_v[disinvalid]+3);
         ((TF1*)(*caftg)[dis])->SetParameter(1,12);
         ((TF1*)(*caftg)[dis])->SetParLimits(1,10,20);
         ((TF1*)(*caftg)[dis])->SetParameter(2,1);
         ((TF1*)(*caftg)[dis])->SetParLimits(2,0.8,4);
         ((TF1*)(*caftg)[dis])->SetParameter(3,integral/15.);
         ((TF1*)(*caftg)[dis])->SetParLimits(3,0,50);
         ((TF1*)(*caftg)[dis])->SetParameter(4,4);
         ((TF1*)(*caftg)[dis])->SetParLimits(4,0,20);
         
         
      }
    ((TF1*)(*caftg)[dis])->SetParNames(
                                       "MPPCPos","MPPCSize","MPPCSigma","Signal","BackGround"
                                       );
    
    cWF->cd(WFvalidch+1);                 // go to the next pad
    double x0init,winit;
    if (scan==1){
      x0init= PMZ_v[ich];
      winit= 5;
    }
    else{
      x0init= PMPhi_v[ich];
      winit=.7;
    }

    ((TH1D*)(*cahsignal)[dis])->Draw("cm(1)");// draw the histogram
    if (TRGGain_v[dis]==255){
      fit->SetParameters(histmax,x0init,winit,20);
      ((TH1D*)(*cahsignal)[dis])->Fit("fit","+","",xmin - 20,xmax+ 20);
      TF1 *fitf = ((TH1D*)(*cahsignal)[dis])->GetFunction("fit");
      amp = fitf->GetParameter(0);
      xcenter = fitf->GetParameter(1);
      w = fitf->GetParameter(2);
      offset = fitf->GetParameter(3);
      sprintf(fitres,"Amp = %5.2f, x0 = %5.2f, w = %5.2f, b = %5.2f",amp,xcenter,w,offset);
      //title->AddText(fitres);
      fitout->AddText(fitres);
      fitout->Draw();

      zlisted = (Float_t) PMZ_v[dis];
      philisted = (Float_t) PMPhi_v[dis];
      idnum = (Float_t) validPM_v[dis];
      fitcenter = (Float_t) xcenter;
      fitNDF = (Float_t) fitf->GetNDF();
      fitChiSquared = (Float_t) fitf->GetChisquare();
      fitProb = (Float_t) fitf->GetProb();
      
      fittuple->Fill(zlisted,philisted,idnum,runnum,fitcenter,fitNDF,fitChiSquared,fitProb);
      fittuple->AutoSave("SaveSelf");
      
      //sprintf(name,"MPPC %d Z %.1f mm Phi %.2f deg Row %.0f Column %.0f Distance %d \n Amp = %5.2f, x0 = %5.2f, w = %5.2f, b = %5.2f",*it,PMZ_v[ich],PMPhi_v[ich],round((PMPhi_v[ich] - phimin)/1.35),round((PMZ_v[ich]- zmin)/15.1), ich,amp,xcenter,w,offset );    ((TH1D*)(*cahsignal)[dis])->SetTitle(name);
    }
    
    WFvalidch++;                          // increment valid MPPC index
    } //----------end loop on valid MPPCs---------------------------------------
  
  
  
  TCanvas* cWF2= new TCanvas("cWF2","cWF2",1500,750);   // canvas for z of MPPC with max charge
  if (Nenabled >=  30)
    cWF2->Divide(6,6);
  else
    cWF2->Divide(4, 6);
  
  gStyle->SetTitleSize(0.06,"t");
  int WFvalidch2=0;
  
  
  //---------------loop on valid MPPCs----------------------------------------
  for (vector<int>::iterator it=validPM_v.begin();it!=validPM_v.end();it++) 
  {
    int dis2=distance(validPM_v.begin(),it);
    const int ich2 = distance(validPM_v.begin(),it);
    
    sprintf(name,"MPPC %d Z %.1f mm Phi %.2f deg Row %.0f Column %.0f Distance %d",*it,PMZ_v[ich2],PMPhi_v[ich2],round((PMPhi_v[ich2] - phimin)/1.35),round((PMZ_v[ich2]- zmin)/15.1), ich2 );
    ((TH1D*)(*detectedcharge)[dis2])->SetTitle(name);
    ((TH1D*)(*detectedcharge)[dis2])->SetTitleSize(0.08);
    ((TH1D*)(*detectedcharge)[dis2])->GetXaxis()->SetLabelSize(0.08);
    ((TH1D*)(*detectedcharge)[dis2])->GetYaxis()->SetLabelSize(0.08);
    ((TH1D*)(*detectedcharge)[dis2])->SetMaximum(50);
    ((TH1D*)(*detectedcharge)[dis2])->SetStats(false);

    
    cWF2->cd(WFvalidch2+1);                     // go to the next pad
    ((TH1D*)(*detectedcharge)[dis2])->Draw();   // draw the histogram
    WFvalidch2++;                               // increment valid MPPC index
  } //----------end loop on valid MPPCs---------------------------------------

  int fitvalch=0;
  
  //---------------loop on valid MPPCs----------------------------------------
 for (vector<int>::iterator it=validPM_v.begin();it!=validPM_v.end();it++) 
    {
      int dis=distance(validPM_v.begin(),it);
      const int ich = distance(validPM_v.begin(),it);

      sprintf(name,"MPPC %d Z %.1f mm Phi %.2f deg Row %.0f Column %.0f Distance %d",*it,PMZ_v[ich],PMPhi_v[ich],round((PMPhi_v[ich] - phimin)/1.35),round((PMZ_v[ich]- zmin)/15.1), ich );
      /*((TH1D*)(*cahsignal)[dis])->SetTitle(name);   
      ((TH1D*)(*cahsignal)[dis])->SetTitleSize(0.08);
      ((TH1D*)(*cahsignal)[dis])->GetXaxis()->SetLabelSize(0.08);
      ((TH1D*)(*cahsignal)[dis])->GetYaxis()->SetLabelSize(0.08);
    if (scan ==1)
      ((TH1D*)(*cahsignal)[dis])->SetMaximum(1000);
    else
      ((TH1D*)(*cahsignal)[dis])->SetMaximum(1000);

      ((TH1D*)(*cahsignal)[dis])->SetStats(false);*/

    double histmax= ((TH1D*)(*cahsignal)[dis])->TH1::GetBinContent(((TH1D*)(*cahsignal)[dis])->TH1::GetMaximumBin());
    Double_t integral =((TH1D*)(*cahsignal)[dis])->Integral();
    Double_t disinvalid = std::distance(validPM_v.begin(),std::find(validPM_v.begin(),validPM_v.end(),*it));
    
      ((TH1D*)(*cahsignald)[dis])->SetTitle(name);   
      ((TH1D*)(*cahsignald)[dis])->SetTitleSize(0.08);
      ((TH1D*)(*cahsignald)[dis])->GetXaxis()->SetLabelSize(0.08);
      ((TH1D*)(*cahsignald)[dis])->GetYaxis()->SetLabelSize(0.08);
    if (scan ==1)
      ((TH1D*)(*cahsignald)[dis])->SetMaximum(1000);
    else
      ((TH1D*)(*cahsignald)[dis])->SetMaximum(1000);

    ((TH1D*)(*cahsignald)[dis])->SetStats(false);

    //((TH1D*)(*cahsignald)[dis])->Add(((TH1D*)(*cahsignal)[dis]),1);

    double histmaxd= ((TH1D*)(*cahsignald)[dis])->TH1::GetBinContent(((TH1D*)(*cahsignald)[dis])->TH1::GetMaximumBin());
    Double_t integrald =((TH1D*)(*cahsignald)[dis])->Integral();
    Double_t disinvalidd = std::distance(validPM_v.begin(),std::find(validPM_v.begin(),validPM_v.end(),*it));
    
    if (scan==0) {
      ((TF1*)(*caftg)[dis])->SetParameter(0,PMPhi_v[disinvalid]);
      ((TF1*)(*caftg)[dis])->SetParLimits(0,PMPhi_v[disinvalid]-0.5,PMPhi_v[disinvalid]+0.5);
      ((TF1*)(*caftg)[dis])->SetParameter(1,1.2);
      ((TF1*)(*caftg)[dis])->SetParLimits(1,1.0,1.4);
         ((TF1*)(*caftg)[dis])->SetParameter(2,0.1);
         ((TF1*)(*caftg)[dis])->SetParLimits(2,0,0.3);
         
         
    }  
    else{
         ((TF1*)(*caftg)[dis])->SetParameter(0,PMZ_v[disinvalid]);
         ((TF1*)(*caftg)[dis])->SetParLimits(0,PMZ_v[disinvalid]-3,PMZ_v[disinvalid]+3);
         ((TF1*)(*caftg)[dis])->SetParameter(1,12);
         ((TF1*)(*caftg)[dis])->SetParLimits(1,10,20);
         ((TF1*)(*caftg)[dis])->SetParameter(2,1);
         ((TF1*)(*caftg)[dis])->SetParLimits(2,0.8,4);
         ((TF1*)(*caftg)[dis])->SetParameter(3,integral/15.);
         ((TF1*)(*caftg)[dis])->SetParLimits(3,0,50);
         ((TF1*)(*caftg)[dis])->SetParameter(4,4);
         ((TF1*)(*caftg)[dis])->SetParLimits(4,0,20);
         
         
      }
    ((TF1*)(*caftg)[dis])->SetParNames(
                                       "MPPCPos","MPPCSize","MPPCSigma","Signal","BackGround"
                                       );
    
    cFitdiffs->cd(fitvalch+1);                 // go to the next pad
    //((TH1D*)(*cahsignal)[dis])->Draw("cm(1)");// draw the histogram
    double x0init,winit;
    if (scan==1){
      x0init= PMZ_v[ich];
      winit= 5;
    }
    else{
      x0init= PMPhi_v[ich];
      winit=.7;
    }
    if (TRGGain_v[dis]==255){
      fit->SetParameters(histmax,x0init,winit,20);
      ((TH1D*)(*cahsignal)[dis])->Fit("fit","","",xmin - 20,xmax+ 20);
      //TF1 *fitd = ((TH1D*)(*cahsignal)[dis])->GetFunction("fit");
      ((TH1D*)(*cahsignald)[dis])->Add(fit,-1);
      ((TH1D*)(*cahsignald)[dis])->Draw();
      fitvalch++;
    }
                             // increment valid MPPC index
    } //----------end loop on valid MPPCs---------------------------------------
  
  //-----------------save histograms to file----------------------------------
  sprintf(name,"cWFxray_r%d.pdf",run);
  cWF->SaveAs(name);
  fout->cd();
  cWF->Write();

  sprintf(name,"cWF2xray_r%d.pdf",run);
  cWF2->SaveAs(name);
  fout->cd();
  cWF2->Write();

  sprintf(name,"cFitSubtractxray_r%d_%d.pdf",run,supergausfitpower);
  cFitdiffs->SaveAs(name);
  fout->cd();
  cFitdiffs->Write();
  
  TCanvas* cbp = new TCanvas("beamposition","beamposition",800,600);
  grBeamPosition->SetMarkerStyle(8);
  grBeamPosition->Draw("ap");
  cbp->Write();
  grBeamPosition->Write("BeamPosition");
  
  fout->Close();  //  close the output file
  
  
  return;
}


//________________________________________________________________________________

void getPMdata(int run,bool WFAnalysis, bool TRAnalysis) {
  //Get all PMs in use
  TFile *frec;
  if (WFAnalysis) 
    frec = new TFile(RecWFFilePath(run).Data(),"READ");
  TClonesArray* pmrhArray = (TClonesArray*)frec->Get("XECPMRunHeader");
  MEGXECPMRunHeader *pmrh = 0;
  for (int iPM = 1; iPM < Nmppc; iPM++) {
    pmrh = (MEGXECPMRunHeader*)(pmrhArray->At(iPM));
    int DCid=pmrh->GetDRSChipID();
    int TRGGain= pmrh->GetTRGGain();
    int ScalerCh = pmrh->GetDRSChipID() * 8 + pmrh->GetDRSChannelOnChip();
    double X_buf = pmrh->GetXYZAt(0) * 10;
    double Y_buf = pmrh->GetXYZAt(1) * 10;
    double Z_buf = pmrh->GetXYZAt(2) * 10;
    double Phi_buf = XYZ2Phi(X_buf,Y_buf,Z_buf);
    
    // if (DCid >= 0)
    //cout << Z_buf << " z " << Phi_buf << " phi " << endl;
   
    //above values have been chosen so that the first value will replace these very large or small values
    //below I've done TRGGain > 0.1 as the triggered values are 0, 1, or 255 so this will pickup all 1 or 255 (triggered)
    if (TRGGain > 0.1) {
      if (Z_buf > zmax)
        zmax = Z_buf;
      if (Z_buf < zmin)
        zmin = Z_buf;
      if (Phi_buf > phimax)
        phimax = Phi_buf;
      if (Phi_buf < phimin)
        phimin = Phi_buf;
    }
    //now that phimin etc are defined I will find valid pms inside this and then one more, i.e. finding surrounding mppc
    //here TTRGain says what MPPCs are triggered on, output of 1 is a negative, 255 is a positive this if statement excludes all but the 16 mppc
  }
  
  for (int iPM = 1; iPM < Nmppc; iPM++) {
    pmrh = (MEGXECPMRunHeader*)(pmrhArray->At(iPM));
     int DCid=pmrh->GetDRSChipID();
     int TRGGain= pmrh->GetTRGGain();
     int ScalerCh = pmrh->GetDRSChipID() * 8 + pmrh->GetDRSChannelOnChip();
     double X_buf = pmrh->GetXYZAt(0) * 10;
     double Y_buf = pmrh->GetXYZAt(1) * 10;
     double Z_buf = pmrh->GetXYZAt(2) * 10;
     double Phi_buf = XYZ2Phi(X_buf,Y_buf,Z_buf);
     double mppc_z_length = 20;
     double mppc_phi_length =1.6; 
     if (  (Z_buf > (zmin - mppc_z_length)) && (Z_buf < (zmax + mppc_z_length)) && (Phi_buf > (phimin - mppc_phi_length)) && (Phi_buf < (phimax + mppc_phi_length)) ) {
       cout << " phi = " << Phi_buf << " z = " << Z_buf <<endl; 
       ScalerCh_map[iPM]=ScalerCh;
       validPM_v.push_back(iPM);
       Nenabled++;
       //nenabled is number of valid mppcs (ie the number of mppcs in the 6x6 array
       // cout << zmin << " z min "<< phimin << " phi min " << endl;
       cout << "Nenabled = " << Nenabled << endl;
       TRGGain_v.push_back(TRGGain);
       PMZ_v.push_back(Z_buf);
       PMPhi_v.push_back(Phi_buf);
       ChTRGGainmap[iPM] = TRGGain;
     }
    
  }
  Nvalidpm=validPM_v.size();
}


bool IsXrayEvent(double Nsum2){
  if (Nsum2<1*TMath::Power(10,7)) {
    return true;
  }else{
    return false;
  }
}

double XYZ2Phi(double x, double y, double /* z */) {
  return x == 0.0 && y == 0.0 ? 0.0 : 180 * degree + TMath::ATan2(-y, -x) * radian;
}

double TwoGaus(double *x,double *par){
  // Function with a flat line and two gaussians
  double xx       =x[0];
  double mean     =par[0];
  double topwidth =par[1];
  double meanfg   =par[0]-par[1]/2;
  double meansg   =par[0]+par[1]/2;
  double sigma    =par[2];
  double height   =par[3];
  double offset   =par[4];
  double f;
  if(xx<meanfg){
    f=offset+height*TMath::Gaus(xx,meanfg,sigma,true)/TMath::Gaus(meanfg,meanfg,sigma,true);
  }else if(xx>=meanfg&&xx<meansg){
    f=offset+ height;
  }else{
    f=offset+ height*TMath::Gaus(xx,meansg,sigma,true)/TMath::Gaus(meansg,meansg,sigma,true);
  }
  return f;
}

Double_t WeightedMean(TGraphErrors* gr){
   Double_t wm=0;
   Int_t Npoints=gr->GetN();
   Double_t x;
   Double_t y;
   Double_t ysum;
   for (Int_t ipt = 0; ipt < Npoints; ipt++) {
     gr->GetPoint(ipt,x,y);
     // std::cout<<"x: "<<x<<" y :"<<y<<std::endl;
      wm  +=x*y;
      ysum+=y;
   }
return wm/ysum;
 
}

 Double_t SuperGaus(Double_t *x,Double_t *par) {
   Double_t arg = 0;
   if (par[2]!=0) arg = (x[0] - par[1])/par[2];
  Double_t fitval = par[0]*TMath::Exp(-TMath::Power(arg,supergausfitpower))+par[3];
  return fitval;
 }



